package com.campanha;

import com.campanha.business.CampanhaService;
import com.campanha.exception.ApiException;
import com.campanha.model.Campanha;
import com.campanha.repository.CampanhaRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class CampanhaApiServiceTest {


    CampanhaService campanhaService;
    CampanhaRepository campanhaRepository;

    @BeforeEach
    public void setup() {
        campanhaRepository = Mockito.mock(CampanhaRepository.class);
        campanhaService = Mockito.mock(CampanhaService.class);
    }

    @Test
    public void campanhaBuscarTest() throws ApiException {
        Mockito.when(campanhaRepository.findById(1L)).thenReturn(null);
        Optional<Campanha> campanha = campanhaService.buscaCampanha(1L);
        Assertions.assertFalse(campanha.isPresent());
    }

    @Test
    public void assosiacaoTest() throws ApiException {
        Mockito.when(campanhaRepository.findByTimeCoracaoTimeCoracaoId(1L)).thenReturn(getListaCampanha());
        List<Optional<Campanha>> optionals = campanhaService.associarClienteTime(1L, 1L);
        Assertions.assertTrue(optionals.size() == 0);
    }

    public List<Optional<Campanha>> getListaCampanha() {
        return null;
    }
}
