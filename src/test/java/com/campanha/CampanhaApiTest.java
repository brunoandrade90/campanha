package com.campanha;

import com.campanha.restservice.CampanhaApi;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CampanhaApiTest {

    @Autowired
    CampanhaApi campanhaApi;

    @BeforeEach
    public void setup() {

    }

    @Test
    public void campanhaApiTest() {
        Assertions.assertNotNull(campanhaApi.listaCampanhas());
        Assertions.assertNotNull(campanhaApi.associarCampanha(1L, 1L));
        Assertions.assertNotNull(campanhaApi.listarPorCliente(2L));

    }


}
