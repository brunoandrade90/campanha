package com.campanha.repository;

import com.campanha.model.Campanha;
import com.campanha.model.TimeCoracao;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TimeCoracaoRepository extends CrudRepository<TimeCoracao, Long> {
    List<Campanha> findByTimeCoracaoId(Long id);

}
