package com.campanha.repository;

import com.campanha.model.ClienteRepresentation;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ClienteRepository extends CrudRepository<ClienteRepresentation, Long> {
   List<ClienteRepresentation> findByClienteId(Long id);

    ClienteRepresentation findByClienteIdAndTimeCoracao_TimeCoracaoId(Long clienteId, Long timeCoracao_timeCoracaoId);

}
