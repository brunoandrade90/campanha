package com.campanha.repository;

import com.campanha.model.Campanha;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface CampanhaRepository extends CrudRepository<Campanha, Long> {

    List<Optional<Campanha>> findByDtVigenciaFimEquals(LocalDate dtVigenciaFim);
    List<Optional<Campanha>> findByTimeCoracaoTimeCoracaoId(Long TimeCoracao_TimeCoracaoId);

}
