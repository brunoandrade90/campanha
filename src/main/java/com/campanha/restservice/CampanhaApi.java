package com.campanha.restservice;

import com.campanha.business.CampanhaService;
import com.campanha.exception.ApiException;
import com.campanha.model.Campanha;
import com.campanha.model.StatusRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/campanha")
public class CampanhaApi {


    private CampanhaService campanhaService;

    private final static Logger log = LoggerFactory.getLogger(CampanhaApi.class);


    @Autowired
    public CampanhaApi(CampanhaService campanhaService) {
        this.campanhaService = campanhaService;
    }

    /**
     * busca campanha por Idcampanha
     *
     * @param idCampanha
     * @return
     */
    @GetMapping(value = "/{id}", produces = "application/json;charset=UTF-8")
    public ResponseEntity getCampanha(@PathVariable(value = "id") Long idCampanha) {
        try {
            return new ResponseEntity(campanhaService.buscaCampanha(idCampanha), HttpStatus.OK);
        } catch (ApiException e) {

            log.error(e.getMessage(), e);
            return new ResponseEntity(convertResponse(false, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /***
     * Endpoint para associacao entre cliente e time.
     * @param idCliente
     * @param idTime
     * @return
     */
    @PostMapping(value = "/associar")
    public ResponseEntity associarCampanha(@RequestParam(value = "idCliente") Long idCliente, @RequestParam(value = "idTime") Long idTime) {
        try {

            return new ResponseEntity(campanhaService.associarClienteTime(idCliente, idTime), HttpStatus.OK);
        } catch (ApiException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity(convertResponse(false, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * listar todas as campanhas
     *
     * @return
     */
    @GetMapping(value = "/listar/todas", produces = "application/json;charset=UTF-8")
    public ResponseEntity listaCampanhas() {
        try {
            return new ResponseEntity(campanhaService.listaCampanhas(), HttpStatus.OK);
        } catch (ApiException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity(convertResponse(false, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /***
     * atualizar Campanha e "posta" em uma fila pra avisar outros recursos.
     * @param campanha
     * @return
     */

    @PutMapping(value = "/atualizar", produces = "application/json;charset=UTF-8")
    public ResponseEntity atualizaCampanha(@RequestBody Campanha campanha){
        try {
            campanhaService.atualizarCampanha(campanha);
            return new ResponseEntity(convertResponse(true, null), HttpStatus.OK);
        } catch (ApiException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity(convertResponse(false, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /***
     *
     *  listar campanhas por time caso precise
     * @param id
     * @return
     */
    @GetMapping(value = "listar/time", produces = "application/json;charset=UTF-8")
    public ResponseEntity listarPorTime(@RequestParam(value = "id") Long id) {
        try {
            return new ResponseEntity(campanhaService.listarPorTime(id), HttpStatus.OK);
        } catch (ApiException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity(convertResponse(false, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /***
     *  listar campanhas por Cliente
     * @param id
     * @return
     */

    @GetMapping(value = "listar/cliente", produces = "application/json;charset=UTF-8")
    public ResponseEntity listarPorCliente(@RequestParam(value = "id") Long id) {
        try {
            return new ResponseEntity(campanhaService.listarPorCliente(id), HttpStatus.OK);
        } catch (ApiException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity(convertResponse(false, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping(value = "inserir", produces = "application/json;charset=UTF-8")
    public ResponseEntity inserirCampanha(@RequestBody Campanha campanha) {
        try {
            campanhaService.inserirCampanha(campanha);
            return new ResponseEntity(convertResponse(true, null), HttpStatus.OK);
        } catch (ApiException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity(convertResponse(false, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * deleta uma campanha existente
     * @param id
     * @return
     */

    @DeleteMapping(value = "/delete", produces = "application/json;charset=UTF-8")
    public ResponseEntity deletarCampanha(@RequestParam Long id) {
        try {
            campanhaService.deletarCampanha(id);
            return new ResponseEntity(convertResponse(true, null), HttpStatus.OK);
        } catch (ApiException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity(convertResponse(false, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private StatusRepresentation convertResponse(boolean response, String erro) {

        StatusRepresentation statusRepresentation = new StatusRepresentation();
        statusRepresentation.setSistema("springboot-campanha");
        if (response) {
            statusRepresentation.setCodigo(0);
            statusRepresentation.setStatus("Executado com Sucesso");
        } else {
            statusRepresentation.setCodigo(1);
            statusRepresentation.setStatus(erro);
        }
        return statusRepresentation;
    }
}
