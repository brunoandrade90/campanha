package com.campanha;

import com.campanha.model.Campanha;
import com.campanha.model.ClienteRepresentation;
import com.campanha.model.TimeCoracao;
import com.campanha.repository.CampanhaRepository;
import com.campanha.repository.ClienteRepository;
import com.campanha.repository.TimeCoracaoRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;

import javax.jms.ConnectionFactory;
import java.time.LocalDate;

@SpringBootApplication
public class CampanhaApplication {

    public static void main(String[] args) {
        SpringApplication.run(CampanhaApplication.class, args);
    }

    /**
     * inserir dados ficticios para a aplicacao
     * @param timeCoracaoRepository
     * @param campanhaRepository
     * @param clienteRepository
     * @return
     */


    @Bean
    CommandLineRunner criaDadosIniciais(TimeCoracaoRepository timeCoracaoRepository, CampanhaRepository campanhaRepository, ClienteRepository clienteRepository) {
        TimeCoracao corinthians = new TimeCoracao("Corinthians");
        TimeCoracao palmeiras = new TimeCoracao("Palmeiras");
        TimeCoracao Santos = new TimeCoracao("Santos");
        TimeCoracao sp = new TimeCoracao("São Paulo");
        TimeCoracao ponte = new TimeCoracao("Ponte Preta");

        timeCoracaoRepository.save(corinthians);
        timeCoracaoRepository.save(palmeiras);
        timeCoracaoRepository.save(Santos);
        timeCoracaoRepository.save(sp);
        timeCoracaoRepository.save(ponte);

        campanhaRepository.save(new Campanha("Campanha 1", corinthians, LocalDate.of(2020, 01, 1), LocalDate.of(2020, 01, 3)));
        campanhaRepository.save(new Campanha("Campanha 2", corinthians, LocalDate.of(2020, 01, 1), LocalDate.of(2020, 01, 4)));

        campanhaRepository.save(new Campanha("Campanha 1", Santos, LocalDate.of(2020, 01, 1), LocalDate.of(2020, 01, 3)));
        campanhaRepository.save(new Campanha("Campanha 2", ponte, LocalDate.of(2020, 01, 1), LocalDate.of(2020, 01, 4)));
        campanhaRepository.save(new Campanha("Campanha 1", ponte, LocalDate.of(2020, 01, 1), LocalDate.of(2020, 01, 3)));
        campanhaRepository.save(new Campanha("Campanha 2", ponte, LocalDate.of(2020, 01, 1), LocalDate.of(2020, 01, 4)));

        campanhaRepository.save(new Campanha("Campanha 3", sp, LocalDate.of(2020, 01, 1), LocalDate.of(2020, 01, 5)));
        campanhaRepository.save(new Campanha("Campanha 4", sp, LocalDate.of(2020, 01, 1), LocalDate.of(2020, 01, 6)));
        ClienteRepresentation clienteRepresentation = new ClienteRepresentation();
        clienteRepresentation.setClienteId(1L);
        clienteRepresentation.setTimeCoracao(corinthians);
        clienteRepository.save(clienteRepresentation);

        return args -> campanhaRepository.save(new Campanha("Campanha 5", sp, LocalDate.of(2020, 01, 1), LocalDate.of(2020, 01, 7)));

    }


}
