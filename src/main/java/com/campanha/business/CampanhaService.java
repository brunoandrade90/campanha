package com.campanha.business;

import com.campanha.exception.ApiException;
import com.campanha.integration.MensageriaCampanhaIntegration;
import com.campanha.model.Campanha;
import com.campanha.model.ClienteRepresentation;
import com.campanha.model.TimeCoracao;
import com.campanha.repository.CampanhaRepository;
import com.campanha.repository.ClienteRepository;
import com.campanha.repository.TimeCoracaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CampanhaService {

    private CampanhaRepository campanhaRepository;
    private TimeCoracaoRepository timeCoracaoRepository;
    private ClienteRepository clienteRepository;
    private MensageriaCampanhaIntegration mensageriaCampanhaIntegration;

    @Autowired
    public CampanhaService(CampanhaRepository campanhaRepository, TimeCoracaoRepository timeCoracaoRepository, ClienteRepository clienteRepository, MensageriaCampanhaIntegration mensageriaCampanhaIntegration) {
        this.campanhaRepository = campanhaRepository;
        this.timeCoracaoRepository = timeCoracaoRepository;
        this.clienteRepository = clienteRepository;
        this.mensageriaCampanhaIntegration = mensageriaCampanhaIntegration;

    }


    public Optional<Campanha> buscaCampanha(Long id) throws ApiException {
        Optional<Campanha> campanha = campanhaRepository.findById(id);
        if (campanha.isPresent()) {
            return campanha;
        }
        throw new ApiException("Campanha nao encontrada");
    }

    public List<Campanha> listaCampanhas() throws ApiException {
        List<Campanha> campanhas = (List<Campanha>) campanhaRepository.findAll();
        if (campanhas.size() > 0) {
            return campanhas;
        }
        throw new ApiException("Lista vazia");
    }


    public void inserirCampanha(Campanha campanha) throws ApiException {
        verificaCampanhaRepetida(campanha);
        if (campanhaRepository.save(campanha) == null) {
            throw new ApiException("Falha ao inserir campanha");
        }
    }

    private void verificaCampanhaRepetida(Campanha campanha) throws ApiException {
        List<Optional<Campanha>> campanhaRepetidalist = campanhaRepository.findByDtVigenciaFimEquals(campanha.getDtVigenciaFim());
        for (Optional<Campanha> campanhaRepetida : campanhaRepetidalist) {
            if (campanhaRepetida.isPresent()) {
                campanhaRepetida.get().setDtVigenciaFim(campanhaRepetida.get().getDtVigenciaFim().plusDays(1));
                atualizaCampanha(campanhaRepetida.get());
            }
        }
    }

    private void atualizaCampanha(Campanha campanha) {
        campanhaRepository.save(campanha);

    }

    public void deletarCampanha(Long id) throws ApiException {
        campanhaRepository.deleteById(id);

    }

    public List<Optional<Campanha>> associarClienteTime(Long idCliente, Long idTime) throws ApiException {
        ClienteRepresentation associacaoExistente = clienteRepository.findByClienteIdAndTimeCoracao_TimeCoracaoId(idCliente, idTime);
        if (associacaoExistente == null) {
            ClienteRepresentation clienteRepresentation = new ClienteRepresentation();
            Optional<TimeCoracao> timeCoracao = timeCoracaoRepository.findById(idTime);
            clienteRepresentation.setTimeCoracao(timeCoracao.get());
            clienteRepresentation.setClienteId(idCliente);
            clienteRepository.save(clienteRepresentation);

            return listarPorCliente(idCliente);
        } else {
            return listarPorCliente(idCliente);
        }


    }

    public List<Optional<Campanha>> listarPorTime(Long id) throws ApiException {
        try {
            List<Optional<Campanha>> campanhas = campanhaRepository.findByTimeCoracaoTimeCoracaoId(id);

            return campanhas;
        } catch (Exception e) {
            throw new ApiException("Erro ao executar Servico");
        }
    }

    public List<Optional<Campanha>> listarPorCliente(Long id) throws ApiException {
        List<ClienteRepresentation> clienteRepresentations = clienteRepository.findByClienteId(id);
        List<Optional<Campanha>> campanhas = new ArrayList<>();
        for (ClienteRepresentation cliente : clienteRepresentations) {
            campanhas.addAll(listarPorTime(cliente.getTimeCoracaoList().getTimeCoracaoId()));
        }
        return campanhas;
    }

    public String atualizarCampanha(Campanha campanha) throws ApiException {
        Campanha campanhaAtualizada = campanhaRepository.save(campanha);
        return mensageriaCampanhaIntegration.enviarCampanhaAtualizada(campanhaAtualizada);

    }
}
