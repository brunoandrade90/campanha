package com.campanha.exception;

import com.campanha.model.StatusRepresentation;

public class ApiException extends Exception {

    StatusRepresentation statusRepresentation;

    public ApiException(String mensagem) {
        super(mensagem);
    }

    public ApiException(String mensagem, Throwable erro) {
        super(mensagem, erro);
    }

}
