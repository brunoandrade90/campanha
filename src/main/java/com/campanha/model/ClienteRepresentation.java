package com.campanha.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/***
 * meu entendimento foi que um cliente poderia ter mais de um time de coracao relacionado, por esse motivo foi criada
 * essa classe para o relacionamento entre 1 cliente varios times.
 *
 *
 */


@Entity
public class ClienteRepresentation {
    @Id
    @GeneratedValue
    private Long id;

    private Long clienteId;

    @OneToOne
    private TimeCoracao timeCoracao;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getClienteId() {
        return clienteId;
    }

    public void setClienteId(Long clienteId) {
        this.clienteId = clienteId;
    }

    public TimeCoracao getTimeCoracaoList() {
        return timeCoracao;
    }

    public void setTimeCoracao(TimeCoracao timeCoracaoList) {
        this.timeCoracao = timeCoracaoList;
    }
}
