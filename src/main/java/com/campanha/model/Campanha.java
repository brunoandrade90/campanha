package com.campanha.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Campanha {

    @Id
    @GeneratedValue
    private Long campanhaId;

    private String nomeCampanha;

    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate dtVigenciaInicio;

    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate dtVigenciaFim;

    @ManyToOne
    private TimeCoracao timeCoracao;


    public Campanha(String nomeCampanha, TimeCoracao timeCoracao, LocalDate dtVigenciaInicio, LocalDate dtVigenciaFim) {
        this.nomeCampanha = nomeCampanha;
        this.dtVigenciaInicio = dtVigenciaInicio;
        this.dtVigenciaFim = dtVigenciaFim;
        this.timeCoracao = timeCoracao;
    }

    public TimeCoracao getTimeCoracao() {
        return timeCoracao;
    }

    public void setTimeCoracao(TimeCoracao timeCoracao) {
        this.timeCoracao = timeCoracao;
    }

    public Long getCampanhaId() {
        return campanhaId;
    }

    public Campanha() {
    }

    public void setCampanhaId(Long campanhaId) {
        this.campanhaId = campanhaId;
    }

    public String getNomeCampanha() {
        return nomeCampanha;
    }

    public void setNomeCampanha(String nomeCampanha) {
        this.nomeCampanha = nomeCampanha;
    }


    public LocalDate getDtVigenciaInicio() {
        return dtVigenciaInicio;
    }

    public void setDtVigenciaInicio(LocalDate dtVigenciaInicio) {
        this.dtVigenciaInicio = dtVigenciaInicio;
    }

    public LocalDate getDtVigenciaFim() {
        return dtVigenciaFim;
    }

    public void setDtVigenciaFim(LocalDate dtVigenciaFim) {
        this.dtVigenciaFim = dtVigenciaFim;
    }
}
