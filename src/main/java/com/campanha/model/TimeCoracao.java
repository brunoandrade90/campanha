package com.campanha.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.List;

@Entity
public class TimeCoracao {

    @Id
    @GeneratedValue
    private Long timeCoracaoId;

    private String nome;



    public TimeCoracao() {
    }


    public Long getTimeCoracaoId() {
        return timeCoracaoId;
    }

    public void setTimeCoracaoId(Long timeCoracaoId) {
        this.timeCoracaoId = timeCoracaoId;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }


    public TimeCoracao(String nome) {
        this.nome = nome;
    }

    public TimeCoracao(String nome, List<Campanha> campanhas) {
        this.nome = nome;

    }
}
