package com.campanha.integration;

import com.campanha.exception.ApiException;
import com.campanha.model.Campanha;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class MensageriaCampanhaIntegration {

    private JmsTemplate jmsTemplate;

    @Autowired
    public MensageriaCampanhaIntegration(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    public String enviarCampanhaAtualizada(Campanha campanha) throws JmsException, ApiException {
        try {

            jmsTemplate.convertAndSend("queue.campanha", campanha);
            return "Mensagem enviada com sucesso";
        } catch (JmsException e) {
            throw new ApiException("Erro ao Enviar mensagem de atualizacao da Campanha");
        }
    }
}
